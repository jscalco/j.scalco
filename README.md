J. Scalco was born in New Orleans, Louisiana. At an early age he knew he wanted to move to Hollywood to join the fun & excitement of music & movie making.
To take a leap of faith by following his heart J.Scalco decided to pack his bags and head for the West Coast, where he landed in Burbank, California.
J.Scalco quickly found himself playing in a rock band, working on national commercials, hit television shows and major motion pictures with bit parts and Under5s, but always dreamed of bigger and better things.
Dissatisfied with the direction his acting and music careers were headed, J.Scalco took a long break. He was determined to learn the ins & outs of both the music industry as well as the film industry. After some pretty well established internships with major companies, J.Scalco decided it was time again to branch out and develop and produce his own material.

Website: https://www.jscalco.com
